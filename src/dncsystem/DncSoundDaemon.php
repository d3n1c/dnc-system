<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace DncSystem;

use DncSystem\DncRemSource;
use DncSystem\DncDataStore;
use DncSystem\DncStorePath;
use SoundControl\Configs;

/**
 * Description of DncSoundDaemon
 *
 * @author denic
 */
class DncSoundDaemon {
  
  const DS = DIRECTORY_SEPARATOR;

  private $configs;
  private $store;
  private $player;
  private $volume;
  private $schema;
  private $storepath;

  public function __construct() {
    $confs = new Configs();
    $this->configs = $confs->configs;
    $this->store = new DncDataStore([
      'storage' => $confs->configs['storepath'],
      'norevs' => $confs->configs['dbstore']['norevs'],
      'database' => [
        'database_name' => $confs->configs['dbstore']['dbname'],
        'username' => $confs->configs['dbstore']['dbuser'],
        'password' => $confs->configs['dbstore']['dbpass'],
        'server' => $confs->configs['dbstore']['dbhost']
      ]
    ]);
    $this->player = new DncRemSource([
      'base_uri' => $confs->configs['audioplayer'],
      'verify' => false,
      'http_errors' => false
    ]);
    $this->volume = new DncRemSource([
      'base_uri' => $confs->configs['volumecontroller'],
      'verify' => false,
      'http_errors' => false
    ]);
    $this->schema = $confs->schema;
    $this->storepath = new DncStorePath();
    unset ($confs);
    $this->prepare();
  }
  
  private function prepare() {
    if (!empty($this->schema)) {
      if (empty($this->store->dataStructure('audio'))) {
        foreach ($this->schema as $type => $structure) {
          $this->store->dataStructure($type, $structure);
        }
      }
    }
    $settings = $this->store->getData('globalsettings', [], 'volumeDefault');
    if (empty($settings['_state'])) {
      $newset = [];
      $newset[] = [
        '_id' => 'volumeDefault',
        '_type' => 'globalsettings',
        '_author' => 'root',
        '_state' => 1,
        'settingvalue' => 50
      ];
      $newset[] = [
        '_id' => 'volumeStep',
        '_type' => 'globalsettings',
        '_author' => 'root',
        '_state' => 1,
        'settingvalue' => 10
      ];
      $newset[] = [
        '_id' => 'smooth',
        '_type' => 'globalsettings',
        '_author' => 'root',
        '_state' => 1,
        'settingvalue' => 1
      ];
      $this->store->updateData($newset);
    }
  }
  
  public function getSpool ($kind = 'infinite') {
    $filename = $this->configs['spoolpath'] . self::DS . $kind;
    if (empty($this->storepath->linesCount($filename))) {
      unset ($filename);
      return;
    }
    $result = file_get_contents($filename);
    $result = explode("\n", $result);
    if (empty($result)) {
      unset ($result);
      unlink($filename);
      return;
    }
    $return = [];
    foreach ($result as $values) {
      if (in_array($kind, ['forced', 'pending', 'playnow', 'infinite'])) {
        if ($kind == 'infinite') {
          $values = base64_decode($values);
        }
        return $kind == 'forced' ? $values : json_decode($values, true);
      }
      $values = json_decode($values, true);
      $return[] = $values;
    }
    if (empty($result)) {
      unlink($filename);
    }
    unset ($result);
    unset ($filename);
    return $return;
  }
  
  public function setSpool (string $kind, array $data = [], int $line = 0) {
    if ($kind == 'infinite') {
      return;
    }
    
    if ($kind == 'forced') {
      if (empty($data[0])) {
        unlink($this->configs['spoolpath'] . self::DS . $kind);
        return true;
      }
      $data = $data[0];
      $this->storepath->writeToPath($this->configs['spoolpath'] . self::DS . $kind, $data, null, true);
      return true;
    }
    
    $legacy = $this->storepath->linesCount($this->configs['spoolpath'] . self::DS . $kind);
    if (empty($data)) {
      if (empty($legacy)) {
        unlink($this->configs['spoolpath'] . self::DS . $kind);
      }
      elseif (!empty($line)) {
        settype($line, 'int');
        if (!empty($line)) {
          $this->storepath->deleteByLines($this->configs['spoolpath'] . self::DS . $kind, [$line]);
        }
      }
      unset($legacy);
      return true;
    }
    unset ($legacy);
    
    settype($line, 'int');
    if (empty($line) ||
          empty($data['list']) ||
          empty($data['_time']) ||
          empty($data['_end']) ||
          !is_string($data['list'])) {
      return;
    }
    
    $this->storepath->writeToPath($path, json_encode($data), $line);
    return true;
  }
  
  public function playnow (string $kind, string $path, string $list = '') {
    $data = [
      'kind' => $kind,
      'list' => $list,
      'path' => $path
    ];
    $data = json_encode($data);
    file_put_contents($this->configs['spoolpath'] . self::DS . 'playnow', $data, LOCK_EX);
    unset ($data);
    return true;
  }

  public function pending (string $kind, string $path, string $list = '') {
    $data = [
      'kind' => $kind,
      'list' => $list,
      'path' => $path
    ];
    $data = json_encode($data);
    file_put_contents($this->configs['spoolpath'] . self::DS . 'pending', $data, LOCK_EX);
    unset ($data);
    return true;
  }
  
  public function smoothSilent () {
    $state = $this->volume->request('/');
    if (empty($state) || $state < 1) {
      unset ($state);
      return;
    }
    for ($i = $state; $i < 1; $i -= $this->configs['volumeStep']) {
      $action = $this->volume->request('/decrease/' . $this->configs['volumeStep']);
      unset ($action);
      sleep(1);
    }
    unset ($state);
    return true;
  }
  
  public function smoothLoud () {
    $state = $this->volume->request('/');
    if (!empty($state) && $state >= $this->configs['defaultVolume']) {
      unset ($state);
      return;
    }
    for ($i = $state; $i >= $this->configs['defaultVolume']; $i += $this->configs['volumeStep']) {
      $action = $this->volume->request('/increase/' . $this->configs['volumeStep']);
      unset ($action);
      sleep(1);
    }
    unset ($state);
    return true;
  }
  
  public function play ($path) {
    if (!empty($this->configs['smooth'])) {
      $this->volume->request('/decrease/100');
    }
    $action = $this->player->request('/play/' . base64_encode($path));
    unset ($action);
    if (!empty($this->configs['smooth'])) {
      $this->smoothLoud();
    }
    return true;
  }
  
  public function stop () {
    if (!empty($this->configs['smooth'])) {
      $this->smoothSilent();
    }
    $action = $this->player->request('/stop');
    unset ($action);
    return true;
  }
  
  public function pause () {
    $action = $this->player->request('/play');
    unset ($action);
    return true;
  }
  
  public function isplaying () {
    $result = $this->player->request('/');
    return empty($result['result']) ? false : true;
  }
  
  public function kindPlay (string $kind, string $path, string $list = null) {
    $line = 0;
    if ($kind == 'priorities') {
      if (empty($list)) {
        unset ($line);
        return;
      }
      $line = $this->storepath->searchByString($this->configs['spoolpath'] . self::DS . $kind, $list, true);
      if (empty($line[0])) {
        unset ($line);
        return;
      }
    }
    settype($line, 'int');
    $playnow = $this->getSpool('playnow');
    if (!empty($this->isplaying())) {
      if (!empty($playnow) &&
              !isset($playnow['kind']) &&
              !isset($playnow['path']) &&
              !isset($playnow['list'])) {
        $this->pending($playnow['kind'], $playnow['path'], $playnow['list']);
      }
      $this->stop();
    }
    $this->play($path);
    if ($kind == 'priorities') {
      $newlist = [];
      $dump = json_decode(base64_decode($list), true);
      foreach ($dump as $values) {
        $check = $this->store->getData('audio', [], $values);
        if (!empty($check['path']) && $check['path'] != $path) {
          $newlist[] = $values;
        }
        unset ($check);
      }
      unset ($dump);
      if (!empty($newlist)) {
        $newlist = base64_encode(json_encode($newlist));
      }
      $gdata = $this->storepath->searchByString($this->configs['spoolpath'] . self::DS . $kind, $list);
      $gdata = $gdata[0];
      $gdata = json_decode($gdata, true);
      $gdata['list'] = $newlist;
      $this->setSpool($kind, $gdata, $line);
      $list = $newlist;
      unset ($gdata, $newlist);
    }
    elseif ($kind == 'forced') {
      $list = null;
      $this->setSpool($kind);
    }
    unset ($line);
    $this->playnow($kind, $path, $list);
    return true;
  }
  
  public function list2Path (string $list) {
    $list = json_decode(base64_decode($list), true);
    if (empty($list)) {
      return;
    }
    $result = [];
    foreach ($list as $values) {
      $audio = $this->store->getData('audio', [], $values);
      if (!empty($audio['path'])) {
        $result[$values] = $audio['path'];
      }
      unset ($audio);
    }
  }
  
}
