<?php

namespace DncSystem;

/**
 * Description of DncStorePath
 *
 * @author denic
 */

class DncStorePath {
  
  const DS = DIRECTORY_SEPARATOR;
  const SED = '/usr/bin/sed';

  public function __construct () {
    
  }
  
  public function preparePath ($path, $isfile = FALSE) {
    $path = str_replace('/', self::DS, $path);
    $path = str_replace('\\', self::DS, $path);
    $path = explode(self::DS, $path);
    if (empty($path)) {
      unset ($path);
      return;
    }
    $result = '';
    foreach ($path as $keys => $values) {
      $result .= self::DS . $values;
      if ($keys < count($path) - 1 && $values != '.' && $values != '..') {
        clearstatcache();
        if (!is_dir($result)) {
          mkdir($result, 0770);
        }
      }
    }
    if (empty($isfile)) {
      clearstatcache();
      if (!is_dir($result)) {
        mkdir($result, 0770);
      }
    }
    return $result;
  }
  
  public function getFilesByPath ($path) {
    clearstatcache();
    if (!is_dir($path)) {
      return;
    }
    $result = scandir($path);
    if (empty($result)) {
      unset ($result);
      return;
    }
    $return = [];
    foreach ($result as $values) {
      if ($values != '.' && $values != '..') {
        $return[] = $values;
      }
    }
    unset ($result);
    return $return;
  }
  
  public function clearPath ($path) {
    clearstatcache();
    if (!is_dir($path)) {
      return;
    }
    $lists = $this->getFilesByPath($path);
    if (empty($lists)) {
      rmdir($path);
      return TRUE;
    }
    foreach ($lists as $values) {
      clearstatcache();
      if (is_file($path . self::DS . $values)) {
        unlink($path . self::DS . $values);
      }
      elseif (is_dir($path . self::DS . $values)) {
        $value = $this->getFilesByPath($path . self::DS . $values);
        if (empty($value)) {
          rmdir($path . self::DS . $values);
        }
        else {
          $this->clearPath($path . self::DS . $values);
        }
      }
    }
    unset ($lists);
    rmdir($path);
    return TRUE;
  }
  
  public function linesCount ($path) {
    clearstatcache();
    if (!is_file($path)) {
      return 0;
    }
    $command = self::SED . ' -n \'$=\' ' . $path;
    $result = shell_exec($command);
    unset ($command);
    if (empty($result)) {
      return 0;
    }
    settype($return, 'int');
    return $result;
  }
  
  public function getStringByLine($path, $line) {
    settype($line, 'int');
    $command = self::SED . ' \'' . $line . '!d\' ' . $path;
    $buffer = shell_exec($command);
    unset ($command);
    return trim($buffer);
  }
  
  public function searchByString ($path, $string, $justlines = FALSE) {
    clearstatcache();
    if (!is_file($path)) {
      return;
    }
    $command = self::SED . ' -n \'/' . $string . '/I=\' ' . $path;
    $lines = shell_exec($command);
    unset ($command);
    if (empty(trim($lines))) {
      unset ($lines);
      return;
    }
    $lines = explode("\n", trim($lines));
    if (empty($lines)) {
      unset ($lines);
      return;
    }
    if (!empty($justlines)) {
      return $lines;
    }
    $result = [];
    foreach ($lines as $values) {
      $dump = $this->getStringByLine($path, $values);
      if (!empty($dump)) {
        $result[$values] = $dump;
      }
    }
    unset ($lines);
    return $result;
  }
  
  public function writeToPath ($path, $string, $line = NULL, $forced = FALSE) {
    $this->preparePath($path, TRUE);
    $lcount = $this->linesCount($path);
    if (empty($line) || $line > $lcount) {
      if (!empty($forced)) {
        file_put_contents($path, $string, LOCK_EX);
      }
      else {
        file_put_contents($path, (empty($lcount) ? NULL : "\n") . $string, FILE_APPEND | LOCK_EX);
      }
      unset ($lcount);
      return TRUE;
    }
    unset ($lcount);
    $command = self::SED . ' -i \'' . $line . 's|^.*$|' . $string . '|\' ' . $path;
    shell_exec($command);
    unset ($command);
    return TRUE;
  }
  
  public function deleteByLines ($path, array $lines = []) {
    if (empty($lines)) {
      return;
    }
    $lcount = $this->linesCount($path);
    if (empty($lcount)) {
      unset ($lcount);
      return;
    }
    $command = self::SED . ' -i \'';
    foreach ($lines as $values) {
      settype($values, 'int');
      if (empty($values) || $values > $lcount) {
        continue;
      }
      $command .= (!empty($comma) ? ';' : '') . $values . 'd';
      $comma = true;
    }
    unset ($lcount, $comma);
    $command .= '\' ' . $path;
    shell_exec($command);
    unset ($command);
    return true;
  }
  
}
