<?php

namespace DncSystem;

use Symfony\Component\HttpFoundation\Request;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Keychain;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Builder;

use DncSystem\DncOpenSSL;
use DncSystem\DncStorePath;

class DncJWT {
  
  const HMAC = 1;
  const RSA = 2;
  const DS = DIRECTORY_SEPARATOR;
  const ALGORITHMs = [
    'HS256', 'HS384', 'HS512', 'RS256', 'RS384', 'RS512', 'ES256', 'ES384', 'ES512', 'PS256', 'PS384'
  ];

  private $httpglobals;
  private $storepath;

  public $configs = [
    'path' => 'jwt',
    'signer' => 2, // 1 = HMAC or 2 = RSA
    'issuer' => 'localhost',
    'audience' => NULL,
    'notbefore' => 3,
    'expiration' => 60 * 60,
    'refresh expiration' => 12,
    'keychain' => [
      'private' => 'private.key',
      'public' => 'public.key',
      'bits' => 2048,
      'passphrase' => NULL
    ]
  ];

  public function __construct () {
    $this->httpglobals = Request::createFromGlobals();
    $this->storepath = new DncStorePath();
  }
  
  private function prepareKey () {
    $sslkey = new DncOpenSSL();
    $sslkey->configs['storepath'] = $this->configs['path'] . self::DS . 'keychain';
    $sslkey->configs['private'] = $this->configs['keychain']['private'];
    $sslkey->configs['public'] = $this->configs['keychain']['public'];
    $sslkey->configs['bits'] = $this->configs['keychain']['bits'];
    $sslkey->configs['passphrase'] = $this->configs['keychain']['passphrase'];
    $sslkey->createKey();
  }
  
  private function tokenID ($timenow, $refresh = FALSE) {
    $audience = empty($this->configs['audience']) ? 'localhost' : $this->configs['audience'];

    $id = [];
    $id[] = $audience;
    $id[] = $timenow;
    if ($refresh !== FALSE) {
      $id[] = rand(1000, 9999);
    }
    $rid = json_encode($id);
    $result = sprintf('%u', crc32($rid));
    unset ($audience, $id, $rid);
    return base_convert($result, 10, 36);
  }
  
  private function getAuthorizationHeader() {
    $result = NULL;
    if (!empty($this->httpglobals->server->get('Authorization'))) {
      $result = trim($this->httpglobals->server->get('Authorization'));
    }
    elseif (!empty($this->httpglobals->server->get('HTTP_AUTHORIZATION'))) {
      $result = trim($this->httpglobals->server->get('HTTP_AUTHORIZATION'));
    }
    elseif (function_exists('apache_request_headers')) {
      $apacheRequestHeaders = apache_request_headers();
      $requestHeaders = array_combine(array_map('ucwords', array_keys($apacheRequestHeaders)), array_values($apacheRequestHeaders));
      if (!empty($requestHeaders['Authorization'])) {
        $result = trim($requestHeaders['Authorization']);
      }
      unset ($requestHeaders, $apacheRequestHeaders);
    }
    return $result;
  }

  public function getBearerToken() {
    $authheader = $this->getAuthorizationHeader();
    if (!empty($authheader)) {
      if (preg_match('/Bearer\s((.*)\.(.*)\.(.*))/', $authheader, $matches)) {
        unset ($authheader);
        return $matches[1];
      }
    }
    unset ($authheader);
    return null;
  }
  
  public function getToken ($info, $refresh = FALSE, $hmacsec = NULL) {
    if (empty($info)) {
      return;
    }

    if (trim(strtolower($this->configs['signer'])) == self::RSA) {
      $keyfile = $this->configs['path'] . self::DS . 'keychain' . self::DS . $this->configs['keychain']['private'];
      $this->storepath->preparePath($keyfile, TRUE);
      $signer = new \Lcobucci\JWT\Signer\Rsa\Sha256();
      $keychain = new Keychain();
      clearstatcache();
      if (!is_file($keyfile)) {
        $this->prepareKey();
        sleep(3);
      }
      $secret = $keychain->getPrivateKey(
        'file://' . $keyfile,
        $this->configs['keychain']['passphrase']
      );
      unset ($keychain, $keyfile);
    }
    else {
      $signer = new \Lcobucci\JWT\Signer\Hmac\Sha256();
      $secret = !empty($hmacsec) ? $hmacsec : $this->configs['keychain']['passphrase'];
    }

    $timenow = time();
    $id = $this->tokenID($timenow, $refresh);
    $expire = empty($refresh) ? 1 : $this->configs['refresh expiration'];
    $issuer = empty($this->configs['issuer']) ? 'localhost' : $this->configs['issuer'];
    $audience = empty($this->configs['audience']) ? 'localhost' : $this->configs['audience'];
    
    $token = (new Builder())->setIssuer($issuer)
        ->setAudience($audience)
        ->setId($id, TRUE)
        ->setIssuedAt($timenow)
        ->setNotBefore($timenow + $this->configs['notbefore'])
        ->setExpiration($timenow + ($this->configs['expiration'] * $expire))
        ->set('coo', $info)
        ->set('refresh', $refresh)
        ->sign($signer, $secret)
        ->getToken();

    unset ($audience, $issuer, $id, $signer, $secret);

    return [
      (string)$token,
      $timenow,
      date('Y-m-d H:i:s', $timenow),
      date('c', $timenow)
    ];
  }
  
  public function getCooClaim ($refresh = FALSE, $allPayloads = FALSE, $hmacsec = NULL) {
    $bearer = $this->getBearerToken();
    $split = explode('.', $bearer);
    if (empty($bearer) || count($split) != 3) {
      unset ($bearer, $split);
      return;
    }
    $algo = json_decode(base64_decode($split[0]), TRUE);
    if (empty($algo['typ'])
        || empty($algo['alg'])
        || !preg_match('/^JWT$/i', $algo['typ'])
        || !in_array($algo['alg'], self::ALGORITHMs)) {
      unset ($bearer, $split);
      return;
    }
    unset ($split);
    
    $token = (new Parser())->parse($bearer);
    unset ($bearer);
    $token->getClaims();

    $audience = empty($token->getClaim('aud')) ? (empty($this->configs['audience']) ? 'localhost' : $this->configs['audience']) : $token->getClaim('aud');
    $vdata = new ValidationData();
    $vdata->setAudience($audience);
    if (!$token->validate($vdata)) {
      unset ($vdata, $token);
      return;
    }
    unset ($vdata, $audience);

    if (trim(strtolower($this->configs['signer'])) == self::HMAC) {
      $signer = new \Lcobucci\JWT\Signer\Hmac\Sha256();
      $secret = !empty($hmacsec) ? $hmacsec : $this->configs['keychain']['passphrase'];
    }
    else {
      $keyfile = $this->configs['path'] . self::DS . 'keychain' . self::DS . $this->configs['keychain']['public'];
      $this->storepath->preparePath($keyfile, TRUE);
      $signer = new \Lcobucci\JWT\Signer\Rsa\Sha256();
      $keychain = new Keychain();
      clearstatcache();
      if (!is_file($keyfile)) {
        $this->prepareKey();
        sleep(3);
      }
      $secret = $keychain->getPublicKey('file://' . $keyfile);
      unset ($keychain, $keyfile);
    }

    if (!$token->verify($signer, $secret)) {
      unset ($signer, $token, $secret);
      return;
    }
    unset ($signer, $secret);

    if ($token->getClaim('refresh') !== $refresh) {
      unset ($token);
      return;
    }

    return !empty($allPayloads) ? $token->getClaims() : (empty($token->getClaim('coo')) ? NULL : $token->getClaim('coo'));
  }
}
