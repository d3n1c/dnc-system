<?php

namespace DncSystem;

use Medoo\Medoo;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Description of DncDataStore
 *
 * @author denic
 */

class DncDataStore {
  
  const SED = '/usr/bin/sed';
  const DS = DIRECTORY_SEPARATOR;
  
  private $database;
  private $storepath;

  private $configs = [
    'storage' => 'datastore',
    'norevs' => []
  ];
  private $dbconfigs = [
    'database_type' => 'mysql',
    'database_name' => 'test',
    'username' => 'root',
    'password' => 'toor',
    'server' => 'localhost'
  ];
  
  public function __construct (array $configs = []) {
    if (!empty($configs)) {
      foreach ($configs as $keys => $values) {
        if ($keys == 'database') {
          foreach ($values as $key => $value) {
            $this->dbconfigs[$key] = $value;
          }
        }
        else {
          $this->configs[$keys] = $values;
        }
      }
    }
    $this->storepath = new DncStorePath();
    $this->database = new Medoo($this->dbconfigs);
    $changes = $this->dataStructure('changes');
    if (empty($changes)) {
      $this->dataStructure('changes', [
        'fields' => [
          'data' => [
            'type' => 'text',
            'required' => TRUE,
            'default' => '',
            'description' => 'Value of the data'
          ]
        ],
        'primary key' => ['_type', '_id']
      ]);
    }
    else {
      $this->database->delete('changes', ['_time[<]' => time() - (60 * 60 * 12)]);
    }
  }
  
  private function allTables() {
    $result = $this->database->query('SHOW TABLES')->fetchAll();
    if (empty($result)) {
      unset ($result);
      return;
    }
    $return = [];
    foreach ($result as $values) {
      if (!empty($values[0])) {
        $return[] = $values[0];
      }
    }
    unset ($result);
    return $return;
  }
  
  public function dataStructure ($type = NULL, array $structure = [], $delete = FALSE) {
    $schemePath = $this->configs['storage'] . self::DS . 'scheme';
    if (!empty($delete)) {
      if (empty($type)) {
        unset ($schemePath);
        return;
      }
      $this->database->query('DROP TABLE ' . $type);
      $schemePath .= self::DS . $type . '.json';
      clearstatcache();
      if (!is_file($schemePath)) {
        unset ($schemePath);
        return;
      }
      unlink($schemePath);
      unset ($schemePath);
      return TRUE;
    }
    
    if (empty($structure)) {
      if (empty($type)) {
        $list = $this->storepath->getFilesByPath($schemePath);
        if (empty($list)) {
          unset ($list, $schemePath);
          return;
        }
        $result = [];
        foreach ($list as $values) {
          $vp = $schemePath . self::DS . $values;
          clearstatcache();
          if (is_file($vp)) {
            $result[] = str_replace('.json', '', $values);
          }
          unset ($vp);
        }
        unset ($list);
        unset ($schemePath);
        return $result;
      }
      
      $schemePath .= self::DS . $type . '.json';
      clearstatcache();
      if (!is_file($schemePath)) {
        unset ($schemePath);
        return;
      }
      $result = file_get_contents($schemePath);
      unset ($schemePath);
      $result = json_decode($result, TRUE);
      return empty($result) ? NULL : $result;
    }
    
    $tables = $this->allTables();
    if (!empty($tables) && in_array($type, $tables)) {
      unset ($tables, $schemePath);
      return;
    }
    unset ($tables);
    
    $dumpstruct = [
      'fields' => [
        '_id' => !empty($structure['fields']['_id']) ? $structure['fields']['_id'] : [
          'type' => 'varchar(255)',
          'required' => TRUE,
          'default' => '',
          'description' => 'ID of data'
        ],
        '_type' => !empty($structure['fields']['_type']) ? $structure['fields']['_type'] : [
          'type' => 'varchar(255)',
          'required' => TRUE,
          'default' => '',
          'description' => 'Type of data'
        ],
        '_time' => !empty($structure['fields']['_time']) ? $structure['fields']['_time'] : [
          'type' => 'varchar(255)',
          'required' => TRUE,
          'default' => '',
          'description' => 'Time of data'
        ],
        '_state' => !empty($structure['fields']['_state']) ? $structure['fields']['_state'] : [
          'type' => 'tinyint(4)',
          'required' => TRUE,
          'default' => '',
          'description' => 'State of data'
        ],
        '_rev' => !empty($structure['fields']['_rev']) ? $structure['fields']['_rev'] : [
          'type' => 'varchar(255)',
          'required' => TRUE,
          'default' => '',
          'description' => 'Revision code of data'
        ],
        '_author' => !empty($structure['fields']['_author']) ? $structure['fields']['_author'] : [
          'type' => 'varchar(255)',
          'required' => TRUE,
          'default' => '',
          'description' => 'Author of last changed'
        ]
      ],
      'primary key' => !empty($structure['primary key']) ? $structure['primary key'] : ['_id']
    ];
    if (!empty($structure['fields'])) {
      foreach ($structure['fields'] as $keys => $values) {
        $dumpstruct['fields'][$keys] = $values;
      }
    }
    $structure = $dumpstruct;
    unset ($dumpstruct);
    $dataStruct = [];
    foreach ($structure['fields'] as $keys => $values) {
      $str = $values['type'] . (!empty($values['required']) ? ' NOT NULL' : NULL);
      if (!empty($values['unique'])) {
        $str .= ' UNIQUE ';
      }
      if (!empty($values['default'])) {
        $str .= ' DEFAULT \'' . $values['default'] . '\'';
      }
      if (!empty($values['description'])) {
        $str .= ' COMMENT \'' . $values['description'] . '\'';
      }
      $dataStruct[$keys] = $str;
      unset ($str);
    }
    $primaryKey = [];
    if (!empty($structure['primary key'])) {
      $primaryKey = $structure['primary key'];
    }
    
    $query = "CREATE TABLE <$type> (";
    foreach ($dataStruct as $keys => $values) {
      $query .= (!empty($comma) ? ', ' : NULL) . "<$keys> $values";
      $comma = TRUE;
    }
    unset ($comma, $dataStruct);
    if (!empty($primaryKey)) {
      $query .= ', PRIMARY KEY (';
      $query .= implode(', ', $primaryKey);
      $query .= ')';
    }
    unset ($primaryKey);
    $query .= ')';
    $query .= ' ENGINE=INNODB';
    $this->database->query($query);
    unset ($query);
    
    $this->storepath->preparePath($schemePath);
    $schemePath .= self::DS . $type . '.json';
    file_put_contents($schemePath, json_encode($structure), LOCK_EX);
    unset ($schemePath);
    
    return $structure;
  }
  
  public function flushStore () {
    $this->dataStructure('changes', [], TRUE);
    $types = $this->dataStructure();
    if (empty($types)) {
      unset ($types);
      return;
    }
    foreach ($types as $values) {
      $this->dataStructure($values, [], TRUE);
    }
    unset ($types);
    sleep(3);
    
    $this->storepath->clearPath($this->configs['storage']);
    return TRUE;
  }
  
  private function requiredFieldsCheck (array $data = [], $complete = FALSE) {
    if (empty($data)
        || empty($data['_type'])
        || !isset($data['_id'])
        || is_null($data['_id'])
        || trim($data['_id']) == ''
        || !isset($data['_state'])
        || empty($data['_author'])) {
      return;
    }
    if (!empty($complete)) {
      if (empty($data['_time'])
          || empty($data['_rev'])) {
        return;
      }
    }
    return $data;
  }
  
  private function getDataFromArchive ($type, $id) {
    $path = $this->configs['storage'] . self::DS . 'revisions' . self::DS . $type . '.txt';
    clearstatcache();
    if (!is_file($path)) {
      unset ($path);
      return;
    }

    $string = '"_id":"' . $id . '"';
    $command = self::SED . ' -n \'/' . $string . '/I=\' ' . $path;
    unset ($string);
    $lines = shell_exec($command);
    unset ($command);
    $lines = explode("\n", $lines);
    if (empty($lines)) {
      unset ($lines);
      return;
    }
    $return = [];
    foreach ($lines as $values) {
      $values = trim($values);
      settype($values, 'int');
      if (!empty($values)) {
        $command = self::SED . ' \'' . $values . '!d\' ' . $path;
        $result = shell_exec($command);
        unset ($command);
        $result = trim($result);
        if (!empty($result)) {
          $result = json_decode($result, TRUE);
          $return[$result['_rev']] = $result;
        }
        unset ($result);
      }
    }
    unset ($path, $lines);
    
    if (empty($return)) {
      unset ($return);
      return;
    }
    krsort($return);
    foreach ($return as $values) {
      return $values;
    }
  }
  
  private function nextRevCode (array $data = []) {
    $start = empty($data['_rev']) ? [0] : explode('-', $data['_rev']);
    $start = empty($start[0]) ? 0 : $start[0];
    settype($start, 'int');
    $result = $start + 1;
    unset ($start);
    $result = str_repeat('0', 4 - strlen($result)) . $result;
    $result .= '-' . md5(json_encode($data) . time());
    return $result;
  }
  
  private function saveRevision (array $data = []) {
    $data = $this->requiredFieldsCheck($data, TRUE);
    if (empty($data)) {
      return;
    }
    if (in_array($data['_type'], $this->configs['norevs'])) {
      return;
    }
    $path = $this->configs['storage'] . self::DS . 'revisions' . self::DS . $data['_type'] . '.txt';
    $this->storepath->preparePath($path, TRUE);
    $count = $this->storepath->linesCount($path);
    file_put_contents($path, (empty($count) ? NULL : "\n") . json_encode($data), FILE_APPEND | LOCK_EX);
    unset ($count, $path);
  }
  
  private function addChange (array $data = []) {
    $data = $this->requiredFieldsCheck($data, TRUE);
    if (empty($data)) {
      return;
    }

    $change = [];
    foreach ($data as $keys => $values) {
      if (in_array($keys, ['_id', '_type', '_time', '_rev', '_author', '_state'])) {
        $change[$keys] = $values;
      }
    }
    $change['data'] = json_encode($data);

    $legacy = $this->database->get('changes', '_id', [
      '_type' => $change['_type'],
      '_id' => $change['_id']
    ]);
    if (!empty($legacy['_id'])) {
      $this->database->update('changes', $change, [
        '_type' => $change['_type'],
        '_id' => $change['_id']
      ]);
    }
    else {
      $this->database->insert('changes', $change);
    }
    unset ($legacy, $change);
    
    return TRUE;
  }
  
  private function checkRequiredBySchemaFields (array $data = []) {
    if (empty($data)
            || empty($data['_type'])) {
      return;
    }
    
    $schema = $this->dataStructure($data['_type']);
    if (empty($schema['fields'])) {
      unset ($schema);
      return;
    }
    
    foreach ($data as $keys => $values) {
      if (!isset($schema['fields'][$keys])) {
        unset ($data[$keys]);
      }
    }
    
    foreach ($schema['fields'] as $keys => $values) {
      if (!preg_match('/^_/i', $keys)) {
        if (!empty($values['required']) && !isset($data[$keys])) {
          unset ($schema);
          return;
        }
      }
    }
    unset ($schema);
    
    return $data;
  }
  
  private function updateSingleData (array $data = []) {
    $data = $this->requiredFieldsCheck($data);
    if (empty($data)) {
      return;
    }
    
    $data = $this->checkRequiredBySchemaFields($data);
    if (empty($data)) {
      return;
    }
    
    $data['_time'] = empty($data['_time']) ? time() : $data['_time'];
    settype($data['_time'], 'int');
    $data['_time'] = empty($data['_time']) ? time() : $data['_time'];
    
    $legacy = $this->database->get($data['_type'], '*', [
      '_id' => $data['_id']
    ]);
    if (empty($legacy)) {
      if (empty($data['_state'])) {
        return TRUE;
      }
      $legacy = $this->getDataFromArchive($data['_type'], $data['_id']);
      if (empty($legacy)) {
        // insert new
        $data['_state'] = 1;
        $data['_rev'] = $this->nextRevCode($data);
        $this->database->insert($data['_type'], $data);
        $this->saveRevision($data);
        $this->addChange($data);
        unset ($legacy);
        return $data;
      }
      $data['_rev'] = $this->nextRevCode($legacy);
      $this->database->insert($data['_type'], $data);
      $this->saveRevision($data);
      $this->addChange($data);
      unset ($legacy);
      return $data;
    }

    if ($legacy['_rev'] != $data['_rev']) {
      unset ($legacy);
      return;
    }

    if (empty($data['_state'])) {
      $author = $data['_author'];
      $data = $legacy;
      $data['_author'] = $author;
      unset ($legacy, $author);
      $this->database->delete($data['_type'], ['_id' => $data['_id']]);
      $data['_state'] = 0;
      $data['_rev'] = $this->nextRevCode($data);
      $this->saveRevision($data);
      $this->addChange($data);
      return TRUE;
    }

    $data['_state'] = 2;
    $data['_rev'] = $this->nextRevCode($data);

    $revision = [];
    foreach ($data as $keys => $values) {
      if (in_array($keys, ['_id', '_type', '_time', '_rev', '_author', '_state'])) {
        $revision[$keys] = $values;
      }
      else {
        if (!isset($legacy[$keys]) || $values != $legacy[$keys]) {
          $revision[$keys] = $values;
        }
      }
      $legacy[$keys] = $values;
    }

    $this->database->update($legacy['_type'], $legacy, ['_id' => $legacy['_id']]);

    // save revision
    $this->saveRevision($revision);
    unset ($revision);

    // add change item
    $this->addChange($legacy);
    
    return $legacy;
  }
  
  public function updateData(array $data = []) {
    if (empty($data)) {
      return;
    }
    $return = [];
    foreach ($data as $keys => $values) {
      $result = $this->updateSingleData($values);
      $return[$keys] = $result;
    }
    return $return;
  }
  
  public function getData($type, array $fields = [], $id = NULL, array $options = [], $justcount = FALSE) {
    $fields = empty($fields) ? '*' : $fields;
    if (!is_null($id) && trim($id) != '') {
      $options = ['_id' => $id];
      $result = $this->database->get($type, $fields, $options);
    }
    elseif (!empty($justcount)) {
      $result = $this->database->count($type, $fields, $options);
    }
    else {
      $result = $this->database->select($type, $fields, $options);
    }

    return $result;
  }
  
  public function getLastDataTime($type) {
    $result = $this->database->query('SELECT MAX(_time) AS _time FROM <' . $type . '>')->fetchAll();
    return empty($result[0]['_time']) ? NULL : $result[0]['_time'];
  }
}