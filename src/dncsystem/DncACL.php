<?php

namespace DncSystem;

use GuzzleHttp\Client;
use DncSystem\DncStorePath;

class DncACL {
  
  const DS = DIRECTORY_SEPARATOR;

  private $author = NULL;
  private $store;
  private $configs = [
    'author' => [
      'server' => 'http://localhost/authors'
    ]
  ];
  private $scheme = [
    'users' => [
      'description' => [
        'type' => 'varchar(255)',
        'required' => TRUE,
        'default' => '',
        'description' => 'Description of user'
      ],
      'roles' => [
        'type' => 'text',
        'required' => TRUE,
        'default' => '',
        'description' => 'Roles of user'
      ]
    ],
    'roles' => [
      'description' => [
        'type' => 'varchar(255)',
        'required' => TRUE,
        'default' => '',
        'description' => 'Description of role'
      ]
    ],
    'permissions' => [
      'description' => [
        'type' => 'varchar(255)',
        'required' => TRUE,
        'default' => '',
        'description' => 'Description of permission'
      ],
      'roles' => [
        'type' => 'text',
        'required' => TRUE,
        'default' => '',
        'description' => 'Roles of permission'
      ]
    ]
  ];
  private $requiredDatas = [
    'roles' => [
      ['_id' => 'anonymous', 'description' => 'Anonymous'],
      ['_id' => 'authenticated', 'description' => 'Authenticated'],
      ['_id' => 'administrator', 'description' => 'Administrator']
    ],
    'users' => [
      ['_id' => 'root', 'description' => 'Root', 'roles' => ['authenticated', 'administrator']]
    ],
    'permissions' => [
      [
        '_id' => 'viewDncACL',
        'description' => 'View some permitted DNC ACL data',
        'roles' => ['administrator']
      ],
      [
        '_id' => 'administerDncACL',
        'description' => 'Administer all DNC ACL data',
        'roles' => ['administrator']
      ]
    ]
  ];
  
  public function __construct ($store, array $configs = []) {
    if (is_object($store)) {
      $this->store = $store;
    }
    if (!empty($configs)) {
      foreach ($configs as $keys => $values) {
        if ($keys != 'author') {
          $this->configs[$keys] = $values;
        }
        else {
          foreach ($values as $key => $value) {
            $this->configs[$keys][$key] = $value;
          }
        }
      }
    }
  }

  private function sendToAuthor($path = NULL, $method = NULL, array $headers = [], array $data = [], $body = NULL, array $query = []) {
    $options = [];
    $configs = [
      'base_uri' => $this->configs['author']['server'] . '/',
      'verify' => FALSE,
      'http_errors' => FALSE
    ];
    if (!empty($headers)) {
      $configs['headers'] = $headers;
    }
    $configs['headers']['Accept'] = 'application/json';
    if (!empty($data)) {
      $configs['headers']['Content-Type'] = 'x-www-form-urlencoded';
      $configs['form_params'] = $data;
    }
    
    if (!empty($body)) {
      $options['body'] = $body;
    }
    if (!empty($query)) {
      $options['query'] = $query;
    }
    $server = new Client($configs);
    $method = !empty($method) ? $method : 'GET';
    $path = !empty($path) ? $path : '/';
    $path = $path == '/' ? '' : preg_replace('/^\//', '', $path);
    $response = $server->request($method, $path, $options);
    unset ($server, $options, $configs);
    $response = [
      'body' => (string)$response->getBody(),
      'headers' => $response->getHeaders(),
      'status' => $response->getStatusCode(),
      'reason' => $response->getReasonPhrase()
    ];
    return $response;
  }
  
  private function checkUserAvailabilty ($id) {
    $response = $this->sendToAuthor('is-exist/' . $id);
    if (empty($response['body']) || $response['status'] != 200) {
      unset ($response);
      return;
    }
    $result = json_decode($response['body'], TRUE);
    unset ($response);
    return empty($result['result']) ? FALSE : $result['result'];
  }
  
  private function prepareUpdatedData(array $data = []) {
    if (empty($data['_id'])
        || empty($data['_type'])) {
      return;
    }
    $legacy = $this->store->getData($data['_type'], [], $data['_id']);
    if (!empty($legacy['_id'])) {
      foreach ($legacy as $keys => $values) {
        if (!in_array($keys, ['_author', '_state']) && !isset($data[$keys])) {
          $data[$keys] = $values;
        }
      }
    }
    $data['_time'] = time();
    $data['_state'] = !isset($data['_state']) ? (empty($legacy['_id']) ? 1 : 2) : $data['_state'];
    $data['_author'] = empty($data['_author']) ? 'root' : $data['_author'];
    $data['_rev'] = empty($data['_rev']) ? 'new-' . md5(time()) : $data['_rev'];
    unset ($legacy);
    
    // check availability
    if ($data['_type'] == 'users' && $data['_state'] == 1) {
      $check = $this->checkUserAvailabilty($data['_id']);
      if (empty($check)) {
        unset ($check);
        return;
      }
      unset ($check);
    }
    
    foreach ($data as $keys => $values) {
      if (is_array($values)) {
        $data[$keys] = json_encode($values);
      }
    }
    return $data;
  }
  
  private function updateData (array $data = []) {
    if (empty($data)) {
      return;
    }
    $update = [];
    foreach ($data as $values) {
      $values = $this->prepareUpdatedData($values);
      if (!empty($values)) {
        $update[] = $values;
      }
    }
    $this->store->updateData($update);
    unset ($update);
    return TRUE;
  }
  
  public function preparing () {
    $structures = $this->store->dataStructure();
    foreach ($this->scheme as $keys => $values) {
      if (empty($structures) || !in_array($keys, $structures)) {
        $this->store->dataStructure($keys, ['fields' => $values]);
      }
    }
    unset ($structures);
    
    foreach ($this->requiredDatas as $keys => $values) {
      $update = [];
      foreach ($values as $value) {
        if (!empty($this->store->getData($keys, [], $value['_id']))) {
          continue;
        }
        $value['_type'] = $keys;
        $update[] = $value;
      }
      $this->updateData($update);
      unset ($update);
    }
    return TRUE;
  }
  
  public function setAuthor ($author) {
    $this->author = $author;
  }
  
  public function clearAuthor () {
    $this->author = null;
  }
  
  public function aclAccess ($permission) {
    if (empty($this->author)) {
      return;
    }
    elseif ($this->author == 'root') {
      return TRUE;
    }
    $data = $this->store->getData('users', [], $this->author);
    if (empty($data['roles'])) {
      unset ($data);
      return;
    }
    $data['roles'] = json_decode($data['roles']);
    $pdata = $this->store->getData('permissions', [], $permission);
    if (empty($pdata['roles'])) {
      unset ($pdata);
      return;
    }
    $pdata['roles'] = json_decode($pdata['roles']);
    $result = array_intersect($data['roles'], $pdata['roles']);
    unset ($data, $pdata);
    return empty($result) ? FALSE : TRUE;
  }
  
  public function assignACL ($type, $id, $description, array $roles = [], $add = TRUE) {
    if (!$this->aclAccess('administerDncACL')
        || !in_array($type, ['roles', 'users', 'permissions'])) {
      return;
    }
    $legacy = $this->store->getData($type, [], $id);
    if (empty($add)) {
      if (empty($legacy['_id'])) {
        unset ($legacy);
        return;
      }
      if ($type == 'roles' && in_array($id, ['anonymous', 'authenticated', 'administrator'])) {
        unset ($legacy);
        return;
      }
      if ($type == 'permissions' && in_array($id, ['viewDncACL', 'administerDncACL'])) {
        unset ($legacy);
        return;
      }
      if ($type == 'users' && $id == 'root') {
        unset ($legacy);
        return;
      }
      $legacy['_state'] = 0;
      $legacy['_author'] = $this->author;
      $this->store->updateData([$legacy]);
      unset ($legacy);
      return TRUE;
    }
    
    $data = [];
    if (!empty($legacy)) {
      $data = $legacy;
      $data['_state'] = 2;
    }
    else {
      $data['_state'] = 1;
      $data['_type'] = $type;
      $data['_id'] = $id;
    }
    unset ($legacy);
    $data['_author'] = $this->author;
    $data['description'] = $description;
    if ($type != 'roles') {
      $data['roles'] = $roles;
      if ($type == 'users') {
        if (empty($data['roles'])
                || !in_array('authenticated', $data['roles'])) {
          $data['roles'][] = 'authenticated';
        }
      }
      else {
        if (empty($data['roles'])
                || !in_array('administrator', $data['roles'])) {
          $data['roles'][] = 'administrator';
        }
      }
      $data['roles'] = json_encode($data['roles']);
    }
    else {
      unset ($data['roles']);
    }
    $this->store->updateData([$data]);
    unset ($data);
    return TRUE;
  }
}