<?php

namespace DncSystem;

use GuzzleHttp\Client;
use DncSystem\DncStorePath;

class DncRemSource {
  
  private $configs = [
    'base_uri' => 'localhost',
    'verify' => FALSE,
    'http_errors' => FALSE,
    'user' => ['root', 'toor'],
    'login_path' => 'token',
    'path' => 'token.json',
    'expiration' => 60 * 60,
    'refresh expiration' => 12,
    'refresh_name' => 'refresh'
  ];
  
  public function __construct (array $configs = []) {
    if (!empty($configs)) {
      foreach ($configs as $keys => $values) {
        $this->configs[$keys] = $values;
      }
    }
  }
  
  private function sendToServer ($path, $method, array $headers = [], array $data = [], $body = NULL, array $query = []) {
    $options = [];
    $configs = [
      'base_uri' => $this->configs['base_uri'] . '/',
      'verify' => $this->configs['verify'],
      'http_errors' => $this->configs['http_errors']
    ];
    if (!empty($headers)) {
      $configs['headers'] = $headers;
    }
    $configs['headers']['Accept'] = 'application/json';
    if (!empty($data)) {
      $configs['headers']['Content-Type'] = 'x-www-form-urlencoded';
      $configs['form_params'] = $data;
    }

    if (!empty($body)) {
      $options['body'] = $body;
    }
    if (!empty($query)) {
      $options['query'] = $query;
    }
    $server = new Client($configs);
    $method = strtoupper($method);
    $path = $path == '/' ? '' : preg_replace('/^\//', '', $path);
    $response = $server->request($method, $path, $options);
    unset ($server, $options, $configs);
    $response = [
      'body' => (string)$response->getBody(),
      'headers' => $response->getHeaders(),
      'status' => $response->getStatusCode(),
      'reason' => $response->getReasonPhrase()
    ];
    return $response;
  }
  
  private function tokens ($refresh = FALSE, $all = FALSE, array $data = []) {
    if (empty($data)) {
      clearstatcache();
      if (!is_file($this->configs['path'])) {
        return;
      }
      $result = file_get_contents($this->configs['path']);
      $result = json_decode($result, TRUE);
      if (empty($result)) {
        unset ($result);
        return;
      }
      $return = empty($refresh) ? $result['token'] : $result[$this->configs['refresh_name']];
      unset ($result);
      return empty($all) ? $return[0] : $return;
    }
    
    if (empty($data['token'])
        || empty($data[$this->configs['refresh_name']])) {
      return;
    }
    $store = new DncStorePath();
    $store->writeToPath($this->configs['path'], json_encode($data), NULL, TRUE);
    unset ($store);
    return TRUE;
  }
  
  private function login () {
    $data = [
      'username' => $this->configs['user'][0],
      'password' => $this->configs['user'][1]
    ];
    $token = $this->sendToServer($this->configs['login_path'], 'POST', [], $data);
    unset ($data);
    if (empty($token['body']) || $token['status'] != 200) {
      unset ($token);
      return;
    }
    $result = json_decode($token['body'], TRUE);
    unset ($token);
    
    if (empty($result['result'])
        || empty($result['result']['token'])) {
      unset ($result);
      return;
    }
    
    $this->tokens(FALSE, FALSE, $result['result']);
    sleep(3);
    return $result['result']['token'];
  }
  
  private function refreshToken () {
    $token = $this->tokens(TRUE, TRUE);
    if (empty($token[1])) {
      // login
      $token = $this->login();
      return $token;
    }
    $age = time() - $token[1];
    if ($age > ($this->configs['expiration'] * $this->configs['refresh expiration'])) {
      unset ($age);
      // login
      $token = $this->login();
      return $token;
    }
    unset ($age);
    $headers = [
      'Authorization' => 'Bearer ' . $token[0]
    ];
    $result = $this->sendToServer($this->configs['login_path'], 'GET', $headers);
    unset ($headers);
    if (empty($result['body']) || $result['status'] != 200) {
      // login
      $result = $this->login();
      return $result;
    }
    $result = json_decode($result['body'], TRUE);
    if (empty($result['result'])) {
      // login
      $result = $this->login();
      return $result;
    }
    
    $data = [
      $this->configs['refresh_name'] => $token,
      'token' => $result['result']
    ];
    unset ($result, $token);
    $this->tokens(FALSE, FALSE, $data);
    sleep(3);
    return $data['token'];
  }
  
  private function getToken ($all = FALSE) {
    $token = $this->tokens(FALSE, TRUE);
    if (empty($token[1])) {
      $token = $this->login();
      if (empty($token)) {
        unset ($token);
        return;
      }
      return empty($all) ? $token[0] : $token;
    }
    $age = time() - $token[1];
    if ($age > $this->configs['expiration']) {
      unset ($age);
      $result = $this->refreshToken();
      if (empty($result)) {
        unset ($result);
        return;
      }
      return empty($all) ? $result[0] : $result;
    }
    unset ($age);
    return empty($all) ? $token[0] : $token;
  }
  
  public function request ($path, $method = 'GET', $authenticated = FALSE, array $data = [], $body = NULL, array $query = [], array $headers = []) {
    if (!empty($authenticated)) {
      $token = $this->getToken();
      if (empty($token)) {
        unset ($token, $headers);
        return;
      }
      $headers['Authorization'] = 'Bearer ' . $token;
      unset ($token);
    }
    $method = empty($method) ? 'GET' : strtoupper($method);
    $result = $this->sendToServer($path, $method, $headers, $data, $body, $query);
    unset ($headers);
    if (empty($result['body']) || $result['status'] != 200) {
      unset ($result);
      return;
    }
    return json_decode($result['body'], TRUE);
  }
}