<?php

namespace DncSystem;

use DncSystem\DncStorePath;

/**
 * Description of DncOpenSSL
 *
 * @author denic
 */
class DncOpenSSL {
  
  const DS = DIRECTORY_SEPARATOR;
  
  private $storepath;

  public $configs = [
    'storepath' => 'keychain',
    'private' => 'private.key',
    'public' => 'public.key',
    'bits' => 2048,
    'passphrase' => NULL
  ];
  
  public function __construct () {
    $this->storepath = new DncStorePath();
  }
  
  public function createKey () {
    // prepare path
    $path = $this->storepath->preparePath($this->configs['storepath']);
    clearstatcache();
    if (is_file($path . self::DS . $this->configs['public'])) {
      unset ($path);
      return;
    }
    
    $key = openssl_pkey_new(
      array(
        'private_key_bits' => $this->configs['bits'],
        'private_key_type' => OPENSSL_KEYTYPE_RSA,
      )
    );
    
    openssl_pkey_export_to_file($key, $path . self::DS . $this->configs['private']);
    $publicKey = openssl_pkey_get_details($key);
    openssl_free_key($key);
    unset ($key);
    $publicKey = $publicKey['key'];
    file_put_contents($path . self::DS . $this->configs['public'], $publicKey, LOCK_EX);
    unset ($publicKey);
    return;
  }
  
  public function get ($param = 'public', $base64Encode = TRUE) {
    if (!is_string($param) || !in_array($param, ['public', 'private'])) {
      return;
    }
    
    // prepare path
    $path = $this->storepath->preparePath($this->configs['storepath']);
    $path .= $this->configs[$param];
    
    clearstatcache();
    if (!is_file($path)) {
      $this->createKey();
      sleep(3);
    }
    $result = file_get_contents($path);
    return empty($base64Encode) ? $result : base64_encode($result);
  }
  
  public function encrypt ($data, $pubkeys) {
    $pubkeys = !is_array($pubkeys) ? [$pubkeys] : $pubkeys;
    $sealed = '';
    $ekeys = [];
    $result = openssl_seal($data, $sealed, $ekeys, $pubkeys);
    if ($result !== false) {
      $return = array(
        'data' => base64_encode($sealed),
        'envkeys' => base64_encode(serialize($ekeys)),
        'pubkeys' => $pubkeys,
      );
      unset ($sealed, $ekeys, $result);
      return base64_encode(json_encode($return));
    }
    unset ($sealed, $ekeys, $result);
  }
  
  public function decrypt ($data) {
    $data = json_decode(base64_decode($data), true);
    if (empty($data['envkeys'])
            || empty($data['data'])
            || empty($data['pubkeys'])) {
      return;
    }
    $pubkey = $this->get('public', false);
    $pt = array_search($pubkey, $data['pubkeys']);
    $data['envkeys'] = unserialize(base64_decode($data['envkeys']));
    $ekey = $data['envkeys'][$pt];
    unset ($pt);
    $data['data'] = base64_decode($data['data']);
    $private = $this->get('private', false);
    $return = '';
    $result = openssl_open($data['data'], $return, $ekey, $private);
    unset ($private, $ekey);
    if ($result !== false) {
      unset ($result);
      return $return;
    }
    unset ($return, $result);
  }
}